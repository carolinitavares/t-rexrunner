package com.example.t_rexrunner;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG = "T-REX";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;
    Bitmap background;
    Bitmap horizontal;
    Bitmap dinosaur;
    Bitmap dinosaur1;
    Bitmap cactus;

    int direction = -1;

    ArrayList<Bitmap> bitmapCactus = new ArrayList<Bitmap>();

    //Touch variables
    int movement = 0;

    // Game gravity
    double positionY, positionX;
    int v0 = 30;

    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------
    int SPEED = 0;
    int SPEED_2 = 0;

    //Speed variable
    int backX = 0;
    int backLimit = 0;

    int maxH = 0;
    int maxHLimit = 0;

    int cactox = 2392;
    int cactoLimit = 0;


    public GameEngine(Context context, int w, int h) {
        super(context);

        //mp=MediaPlayer.create(context,R.raw.press); //carol

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(background, this.screenWidth, this.screenHeight, false);

        horizontal = BitmapFactory.decodeResource(context.getResources(), R.drawable.horizon);
        horizontal = Bitmap.createScaledBitmap(horizontal, this.screenWidth, this.screenHeight / (25), false);

        dinosaur = BitmapFactory.decodeResource(context.getResources(), R.drawable.dinosaur1);
        dinosaur = Bitmap.createScaledBitmap(dinosaur, 180, 200, false);

        dinosaur1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dinosaur2);
        dinosaur1 = Bitmap.createScaledBitmap(dinosaur1, 180, 200, false);

        cactus = BitmapFactory.decodeResource(context.getResources(), R.drawable.cactus_large);
        cactus = Bitmap.createScaledBitmap(cactus, 480, 200, false);

        bitmapCactus.add(cactus);
        bitmapCactus.add(dinosaur);

        positionX = dinosaur.getWidth();
        positionY = 815;
//
//        Log.d("BITMAP","ARRAY" + dinosaur.getHeight());
//        Log.d("BITMAP","ARRAY" + dinosaur.getWidth());

        this.printScreenInfo();

    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    public void drawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.BLACK);
            paintbrush.setColor(Color.BLACK);

            canvas.drawBitmap(this.background, backX, 0, null);
            canvas.drawBitmap(this.background, backLimit, 0, null);

            canvas.drawBitmap(this.horizontal, maxH, 1000, null);
            canvas.drawBitmap(this.horizontal, maxHLimit, 1000, null);

            canvas.drawBitmap(dinosaur, 100, (float) positionY, paintbrush);
            canvas.drawBitmap(this.cactus, cactox, 815, null);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.drawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    public void updatePositions() {

        if (movement == 1) {
            //Log.d(TAG, "UPDATE");
            //Log.d(TAG, "MOVEMENT = " + movement);
            this.rexMovement();
        }
    }


    public void rexMovement() {
        backX = backX - SPEED;

        backLimit = this.background.getWidth() - (-this.backX);

        if (backLimit <= 0) {
            this.backX = 0;
        }

        maxH = maxH - SPEED_2;
        maxHLimit = this.horizontal.getWidth() - (-this.maxH);

        if (maxHLimit <= 0) {
            this.maxH = 0;
        }


        cactox = cactox - 25;
        cactoLimit = this.cactus.getWidth() - (-this.cactox);
        if (cactoLimit <= 0) {
            this.cactox = 2392;
        }

        if (direction == 1) {
            //Log.d(TAG, "ENTREI NO PULO");
            //make a jump
            positionY += v0 * -3;
            this.drawSprites();

            /*Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Log.d(TAG, "ESPERA 5 SEGUNDOS");
                }
            }, 5000);*/
            for (int i = 0 ; i < 15; i++){
                this.setFPS();
            }

            positionY = 815;
            direction = -1;
        }
    }


    public void setFPS() {
        try {
            gameThread.sleep(50);
        } catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();

        //Log.d(TAG, "Entrei no touch");

        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_UP) {
            this.SPEED = 25;
            this.SPEED_2 = 150;

            movement = 1;
            direction += 1;

            this.updatePositions();
        }

        return true;
    }

    /*@Override
    public boolean onDoubleTap(MotionEvent event) {
        int userAction = event.getActionMasked();

        if (userAction == MotionEvent.ACTION_UP){
            direction = 1;
        }

        Log.d(TAG, "onDoubleTap: " + event.toString());
        return true;
    }*/
}